{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Higher Order SVD / Tucker"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"preamble.jl\"); # be sure to run this segment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "A **Tucker decomposition** of a $d$-th order tensor \n",
    "$$T \\in \\mathbb{R}^{n_1 \\times \\ldots \\times n_d}$$\n",
    "is given through a tensor\n",
    "$$C \\in \\mathbb{R}^{r_1 \\times \\ldots \\times r_d}, \\quad r \\in \\mathbb{N}^d,$$\n",
    "as well as $d$ matrices \n",
    "$$U^{\\mu} \\in \\mathbb{R}^{n_\\mu \\times r_\\mu}, \\quad \\mu = 1,\\ldots,d,$$\n",
    "subject to a certain **representation map** that maps these factors $(U,C)$ to the decomposed tensor, which we here just write as \n",
    "$$T = \\tau(C,U).$$\n",
    "When certain orthogonality related conditions are fulfilled, then the decomposition is also called **HOSVD** - **Higher Order Singular Value Decomposition**. We will get to these conditions later.\n",
    "\n",
    "We can write this map directly via simple summations\n",
    "$$T_{\\alpha_1,\\ldots,\\alpha_d} = \\tau(U,C)_{\\alpha_1,\\ldots,\\alpha_d} := \\sum_{\\beta_1 = 1}^{r_1} \\ldots \\sum_{\\beta_d = 1}^{r_d} U^{1}_{\\alpha_1,\\beta_1} \\cdot \\ldots \\cdot U^{d}_{\\alpha_d,\\beta_d} \\cdot C_{\\beta_1,\\ldots,\\beta_d}, \\quad \\forall \\alpha_\\mu \\in \\{1,\\ldots,n_\\mu\\},\\ \\mu = 1,\\ldots,d.$$ \n",
    "While straightforward, this however does not yet tell us a lot about the structure behind this map."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Unfoldings\n",
    "\n",
    "A useful tool in this context are so called **unfoldings**, also called **matricizations**. \n",
    "\n",
    "**Definition**: Let $\\mu \\in \\{1,\\ldots,d\\}$ and $T \\in \\mathbb{R}^{k_1 \\times \\ldots \\times k_d}$. Then $T^{(\\mu)} \\in \\mathbb{R}^{k_\\mu \\times \\prod_{s \\neq \\mu} k_s}$ is the unique, isomorphic multilinear map induced via\n",
    "$$ (e_{i_1} \\otimes \\ldots \\otimes e_{i_d})^{(\\mu)} = e_{i_\\mu} \\cdot \\mathrm{vec}(\\bigotimes_{\\nu = 1,\\ \\nu \\neq \\mu}^d e_{i_\\nu})^T$$\n",
    "where each $e_j$ is the $j$-th unit vector and $\\mathrm{vec}(\\cdot)$ is the vectorization (in column-first/colexicographical order).\n",
    "\n",
    "While this formal definition is a little indirect, all we do is rearrange entries. It can quickly be understood by observing what it does as we will do in the following.\n",
    "\n",
    "The function `mu_unfold(mu,T)` expects an integer `mu` and a tensor `T`, and returns the matrix $T^{(\\mu)}$. Do not yet look at the code behind it - this will be part of a later exercise. As Julia works column-first, the map $\\mathrm{vec}(\\cdot)$ is simply given by the operation `[:]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8-element Array{Int64,1}:\n",
       " 1\n",
       " 2\n",
       " 3\n",
       " 4\n",
       " 5\n",
       " 6\n",
       " 7\n",
       " 8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T = zeros(Int,2,2,2);\n",
    "T[:] = 1:length(T);\n",
    "T[:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×2 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 1  3\n",
       " 2  4\n",
       "\n",
       "[:, :, 2] =\n",
       " 5  7\n",
       " 6  8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×4 Array{Int64,2}:\n",
       " 1  3  5  7\n",
       " 2  4  6  8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mu_unfold(1,T) # if you got an error here, you likely did not run the very first segment including the preamble"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×4 Array{Int64,2}:\n",
       " 1  2  5  6\n",
       " 3  4  7  8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mu_unfold(2,T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×4 Array{Int64,2}:\n",
       " 1  2  3  4\n",
       " 5  6  7  8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mu_unfold(3,T)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 1 (theory) </font>\n",
    "\n",
    "What is the return value of the function call `mu_unfold(4,B)` for belows tensor $B \\in \\mathbb{R}^{2 \\times 2 \\times 2 \\times 2}$? Do not evaluate it until you think you got the correct answer written down."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×2×2 Array{Int64,4}:\n",
       "[:, :, 1, 1] =\n",
       " 1  3\n",
       " 2  4\n",
       "\n",
       "[:, :, 2, 1] =\n",
       " 5  7\n",
       " 6  8\n",
       "\n",
       "[:, :, 1, 2] =\n",
       "  9  11\n",
       " 10  12\n",
       "\n",
       "[:, :, 2, 2] =\n",
       " 13  15\n",
       " 14  16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "B = zeros(Int,2,2,2,2);\n",
    "B[:] = 1:length(B);\n",
    "B"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mu_unfold(4,B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### <font color = #612158> Exercise 2 (coding) </font>\n",
    "\n",
    "Open the file `my/my_mu_unfold.jl`, fill in the missing parts to write your own version. Test it and compare the finished code to the provided solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Folding\n",
    "\n",
    "The inverse of an unfolding may be referred to as **folding** or **tensorization**. Its implementation requires some more work as we need to provide the size of the resulting tensor. \n",
    "\n",
    "Consequently, the function `mu_fold(k,mu,M)` expects a tuple `k` of integers, the mode `mu` and (a matrix) `M` and returns the tensor $T \\in \\mathbb{R}^{k_1 \\times \\ldots \\times k_{end}}$ for which $M = T^{(\\mu)}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×3 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 1  3\n",
       " 2  4\n",
       "\n",
       "[:, :, 2] =\n",
       " 5  7\n",
       " 6  8\n",
       "\n",
       "[:, :, 3] =\n",
       "  9  11\n",
       " 10  12"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T = zeros(Int,2,2,3);\n",
    "T[:] = 1:length(T);\n",
    "T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×6 Array{Int64,2}:\n",
       " 1  2  5  6   9  10\n",
       " 3  4  7  8  11  12"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "M = mu_unfold(2,T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 2, 3)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "k = size(T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×3 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 1  3\n",
       " 2  4\n",
       "\n",
       "[:, :, 2] =\n",
       " 5  7\n",
       " 6  8\n",
       "\n",
       "[:, :, 3] =\n",
       "  9  11\n",
       " 10  12"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mu_fold(k,2,M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## The $\\times_\\mu$-product\n",
    "\n",
    "The $\\times_\\mu$-product will bring us towards a more structured description of the map $\\tau$ and thus the HOSVD.\n",
    "\n",
    "**Definition**: Let $\\mu \\in \\{1,\\ldots,d\\}$, $T \\in \\mathbb{R}^{k_1 \\times \\ldots \\times k_d}$ and $M \\in \\mathbb{R}^{\\ell_\\mu \\times k_\\mu}$. Then\n",
    "$$ (M \\times_\\mu T)_{\\alpha_1,\\ldots,\\alpha_d} := \\sum_{\\beta_\\mu = 1}^{k_\\mu} M_{\\alpha_\\mu,\\beta_\\mu} \\cdot T_{\\alpha_1,\\ldots,\\alpha_{\\mu-1},\\beta_\\mu,\\alpha_{\\mu+1},\\ldots,\\alpha_d}. $$\n",
    "\n",
    "This summation can in turn be written as a matrix multiplication using the previously introduced unfolding. That is, we have\n",
    "$$ (M \\times_\\mu T)^{(\\mu)} = M \\times T^{(\\mu)}. $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 3 (coding) </font>\n",
    "\n",
    "The function `mu_mode_prod(mu,M,T)` expects an integer `mu`, (a matrix) `M` and a tensor `T`, and returns the tensor $M \\times_\\mu T$.\n",
    "\n",
    "Open the file `my/my_mu_mode_prod.jl` and fill in the missing parts to obtain your own version. Use `mu_unfold` or your own code as well as `mu_fold`.\n",
    "\n",
    "The first thing we do is to observe that $M = 2 I_{k_\\mu}$ should just double all entries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×3 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 2  6\n",
       " 4  8\n",
       "\n",
       "[:, :, 2] =\n",
       " 10  14\n",
       " 12  16\n",
       "\n",
       "[:, :, 3] =\n",
       " 18  22\n",
       " 20  24"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T = zeros(Int,2,2,3);\n",
    "T[:] = 1:length(T);\n",
    "\n",
    "M = 2*eye(Int,2,2);\n",
    "\n",
    "mu = 2;\n",
    "\n",
    "mu_mode_prod(mu,M,T)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In turn, two successive calls where $M_1 = B^{-1}$ and $M_2 = B$ should result in the initial tensor *(why?)*, at least up to round-off errors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×3 Array{Float64,3}:\n",
       "[:, :, 1] =\n",
       " 1.55431e-15  -8.88178e-16\n",
       " 4.44089e-16   2.22045e-15\n",
       "\n",
       "[:, :, 2] =\n",
       " -1.77636e-15   0.0        \n",
       " -8.88178e-16  -1.77636e-15\n",
       "\n",
       "[:, :, 3] =\n",
       " -1.77636e-15  -3.55271e-15\n",
       " -1.77636e-15  -3.55271e-15"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "M_1 = randn(3,3);\n",
    "M_2 = inv(M_1);\n",
    "T - mu_mode_prod(3,M_2, mu_mode_prod(3,M_1,T) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 4 (theory) </font>\n",
    "\n",
    "Let $\\mu,\\nu$ be fixed. When does $M_2 \\times_\\mu (M_1 \\times_\\nu T)$ commute and when can the product be simplified?\n",
    "\n",
    "What is the relation to matrix products of the form $M_1 \\ T \\ M_2 $ when $T$ has order $2$ (thus being a matrix)?\n",
    "\n",
    "Feel free to experiment with the implementation. Visualize the different constellations as tensor networks. You can find a solution in `solutions/Exercise_1_4.ipynb`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## The respresentation map $\\tau$\n",
    "\n",
    "With the $\\times_\\mu$-product, we are now ready to rewrite the unstructured summation into something that is not only shorter mathematically, but will likewise yield a quite short implementation.\n",
    "\n",
    "That is, we have\n",
    "\n",
    "$$T_{\\alpha_1,\\ldots,\\alpha_d} = \\tau(U,C)_{\\alpha_1,\\ldots,\\alpha_d} := \\sum_{\\beta_1 = 1}^{r_1} \\ldots \\sum_{\\beta_d = 1}^{r_d} U^{1}_{\\alpha_1,\\beta_1} \\cdot \\ldots \\cdot U^{d}_{\\alpha_d,\\beta_d} \\cdot C_{\\beta_1,\\ldots,\\beta_d} = U^1 \\times_1 (U^2 \\times_2 \\ldots (U^d \\times_d C)) \\quad =: (U^1,\\ldots,U^d) \\times C.$$\n",
    "\n",
    "Given the commutativity rules considered above, it is very convenient to use the even shorter depiction as done in the last step.\n",
    "\n",
    "We first need to agree on how to arrange the data that describes the mathematical objects $U$ and $C$. Naturally, one could make use of structs here, but we avoid more programming concepts for simplicity.\n",
    "\n",
    "We let $U$ just be an array of matrices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Array{Array{Float64,2},1}:\n",
       " [-0.0576081 -0.395835; -0.419089 0.40011; -0.439184 1.06648]                                                      \n",
       " [-1.00949 1.30532; -0.463843 -2.20173; -0.143051 0.991289; -0.917974 -2.55856]                                    \n",
       " [-0.0209655 -1.05706 -0.699314; 1.39241 0.195071 1.12368; -1.61728 0.560444 0.447811; -1.9048 -0.226352 -0.852783]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "r = (2,2,3);\n",
    "n = (3,4,4);\n",
    "U = [randn(n[1],r[1]),randn(n[2],r[2]),randn(n[3],r[3])];\n",
    "U"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×2 Array{Float64,2}:\n",
       " -0.0576081  -0.395835\n",
       " -0.419089    0.40011 \n",
       " -0.439184    1.06648 "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "U[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The object $C$ instead is again a tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×3 Array{Float64,3}:\n",
       "[:, :, 1] =\n",
       "  0.0822259  2.32576\n",
       " -1.17682    1.23362\n",
       "\n",
       "[:, :, 2] =\n",
       " -1.0817     1.36493\n",
       " -0.568689  -1.49817\n",
       "\n",
       "[:, :, 3] =\n",
       " -0.944019  -0.933572\n",
       "  0.820011   0.449472"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "C = randn(r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 5 (coding) </font>\n",
    "\n",
    "The function `tau(U,C)` expects an array `U` of matrices and a tensor `C`, and returns the tensor $\\tau(U,C)$.\n",
    "\n",
    "Open the file `exercises/my_tau.jl` and fill in the missing parts yourself to obtain your own version. Use `mu_mode_prod` or your own code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3, 4, 4)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T = tau(U,C);\n",
    "size(T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3, 4, 4)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can observe, while the size of $C$ is $r$, the resulting tensor $T = \\tau(U,C)$ has correct size $n$.\n",
    "\n",
    "However, while each $U^\\mu$, $\\mu = 1,\\ldots,d$, as well as $C$ is *generic*, the tensor $T = \\tau(U,C)$ is not. This becomes obvious by looking at the ranks of all unfoldings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, 2, 3)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "modes = tuple(1:length(n)...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3, 4, 4)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "R = randn(n);\n",
    "r_R = map(mu->rank(mu_unfold(mu,R)),modes) # same as: r_R = (rank(mu_unfold(1,R)),rank(mu_unfold(2,R)),rank(mu_unfold(3,R)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 2, 3)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "r_T = map(mu->rank(mu_unfold(mu,T)),modes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3, 4, 4)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "(2, 2, 3)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "display(n)\n",
    "display(r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 6 (theory) </font>\n",
    "\n",
    "Why is the collection of ranks of the unfoldings of $T = \\tau(U,C)$ equal to the size of the *generic* tensor $C$ given *generic* $U^{(\\mu)}$, for $\\mu = 1,\\ldots,d$?\n",
    "\n",
    "Consider *(and prove)* therefor the identity\n",
    "$$ (U \\times C)^{(\\mu)} = U^{\\mu} \\ C^{(\\mu)} \\ (\\bigotimes_{\\nu = 1,\\ \\nu \\neq \\mu}^d U^\\nu)^T. $$\n",
    "Note that this structure is very similar to the initial, formal definition of $\\mu$-unfoldings. You can find the solution in `solutions/Exercise_1_6.ipynb`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## SVD\n",
    "\n",
    "Not only the properties of the HOSVD are related to the SVD, but its calculation can likewise be based on it. We will here make use of the *compact* SVD. \n",
    "\n",
    "By this, we mean to decompose any rank $r$ matrix $M \\in \\mathbb{R}^{k_1 \\times k_2}$ into \n",
    "$$M = U_r \\ \\mathrm{diag}(s_r) \\ V_{r}^T, \\quad U_{r} \\in \\mathbb{R}^{k_1 \\times r}, \\ s_{r} \\in \\mathbb{R}_{\\geq 0}^r,\\ V_{r} \\in \\mathbb{R}^{k_2 \\times r},$$\n",
    "for (column-)orthogonal $U_{r}$ and $V_{r}$.\n",
    "\n",
    "As $U_r$ and $V_r$ are the first $r$ columns of $U$ and $V$, respectively, of the ordinary SVD, this is also how we proceed coding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "r = 2\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "4×3 Array{Float64,2}:\n",
       " 1.0  5.0   9.0\n",
       " 2.0  6.0  10.0\n",
       " 3.0  7.0  11.0\n",
       " 4.0  8.0  12.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "M = zeros(Float64,4,3);\n",
    "M[:] = 1:length(M);\n",
    "\n",
    "r = rank(M);\n",
    "println(\"r = $r\")\n",
    "\n",
    "(U,s,V) = svd(M);\n",
    "U*Diagonal(s)*V'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Float64,2}:\n",
       "  1.0          -2.08293e-16  -8.3916e-17 \n",
       " -2.08293e-16   1.0          -5.69538e-17\n",
       " -8.3916e-17   -5.69538e-17   1.0        "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "U'*U"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Float64,2}:\n",
       " 1.0           0.0           5.55112e-17\n",
       " 0.0           1.0          -5.55112e-17\n",
       " 5.55112e-17  -5.55112e-17   1.0        "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "V'*V"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Array{Float64,1}:\n",
       " 25.436835633480253    \n",
       "  1.722612247521063    \n",
       "  4.514392810132522e-16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "r"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, round-off errors provide a trap in that we can not simply determine $r$ by the number of non-zero singular values. Instead, one should roughly rely on the machine accuracy and the norm of the matrix, as well as possibly the size of the matrix. While the most sensible evaluation depends on ones context, the following provides an acceptable approach here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "machine epsilon equals 2.220446049250313e-16\n",
      "numeric rank equals 2\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "4×3 Array{Float64,2}:\n",
       " 1.0  5.0   9.0\n",
       " 2.0  6.0  10.0\n",
       " 3.0  7.0  11.0\n",
       " 4.0  8.0  12.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "println(\"machine epsilon equals $(eps())\")\n",
    "b = s / norm(s) .> eps()\n",
    "println(\"numeric rank equals $(count(b))\")\n",
    "\n",
    "U_r = U[:,1:r];\n",
    "s_r = s[1:r];\n",
    "V_r = V[:,1:r];\n",
    "\n",
    "U_r*Diagonal(s_r)*V_r'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## HOSVD\n",
    "\n",
    "We now arrive at the central statement for the HOSVD.\n",
    "\n",
    "Let therefor\n",
    "\n",
    "$$ \\mathcal{V}_{\\leq r} := \\{ T \\in \\mathbb{R}^{n_1 \\times  \\ldots \\times n_d} \\mid \\mathrm{rank}(T^{(\\mu)}) \\leq r_\\mu,\\ \\mu = 1,\\ldots,d \\}$$\n",
    "\n",
    "be the set (more specifically, variety) of tensors with ranks lower or equal $r = (r_1,\\ldots,r_d)$.\n",
    "\n",
    "**Theorem** Let $n,r \\in \\mathbb{N}^{d}$. Then\n",
    "$$\\tau: (\\times_{\\mu = 1}^d \\mathbb{R}^{n_\\mu \\times r_\\mu}) \\times \\mathbb{R}^{r_1 \\times \\dots \\times r_d} \\rightarrow \\mathcal{V}_{\\leq r}$$\n",
    "is well-defined and surjective.\n",
    "\n",
    "In other words, if the ranks of the unfoldings of a tensor are entrywise lower than $r$, then there exist $U^\\mu \\in \\mathbb{R}^{n_\\mu \\times r_\\mu}$, $\\mu = 1,\\ldots,d$, and $C \\in \\mathbb{R}^{r_1 \\times \\dots \\times r_d}$ for which $T = \\tau(U,C)$. The previously investigated statement about ranks thus holds true in reverse as well.\n",
    "\n",
    "**Proof.** The well-definedness part is exercise 6. Let $T \\in \\mathcal{V}_{\\leq r}$. Then by definition, for each $\\mu = 1,\\ldots,d$, there exists a compact SVD $T^{(\\mu)} = U^{\\mu}_{r_\\mu} \\mathrm{diag}(s^{\\mu}_{r_\\mu}) {V^{\\mu}}^T$. Due to orthogonality, $T^{(\\mu)} = U^{\\mu}_{r_\\mu} {U^{\\mu}_{r_\\mu}}^T T^{(\\mu)}$, or equivalently, $T = U^{\\mu}_{r_\\mu} \\times_\\mu ({U^{\\mu}_{r_\\mu}}^T \\times_\\mu  T)$. Using the commutativity regarding $\\times_\\mu$ and $\\times_\\nu$, $\\mu \\neq \\nu$, we may thus write $T = (U^{1}_{r_1},\\ldots,U^{d}_{r_d}) \\times ({U^{1}_{r_1}}^T,\\ldots,{U^{d}_{r_d}}^T) \\times T$. By definining $C = ({U^{1}_{r_1}}^T,\\ldots,{U^{d}_{r_d}}^T) \\times T$, we derive at the desired statement $T = \\tau(U,C)$.\n",
    "\n",
    "This proof in fact not only constructs a Tucker decomposition, but an actual HOSVD in that the matrices $U^\\mu$, $\\mu = 1,\\ldots,d$, are the left-singular vectors of the matricizations of $T$. Note that in the following decompositions, we skip the indices $(\\cdot)_{r_\\mu}$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 7 (theory) </font>\n",
    "\n",
    "Show that $\\tau$ (as in the theorem) is generally not injective (even in a non trivial way). You can find the solution in `solutions/Exercise_1_7.ipynb`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 8 (coding) </font>\n",
    "\n",
    "Construct an example demonstrating the result of the prior exercise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 70,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 9 (coding) </font>\n",
    "\n",
    "The function `HOSVD(T)` expects a tensor `T` and returns a pair `(U,C)` that provides a proper input to the function `tau`, and for which $T = \\tau(U,C)$.\n",
    "\n",
    "Open the file `exercises/my_HOSVD.jl` and fill in the missing parts to write your own version. You can use the proof of aboves theorem as a guideline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 71,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = zeros(Float64,4,3,2);\n",
    "d = ndims(T);\n",
    "T[:] = 1:length(T);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 72,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4×3×2 Array{Float64,3}:\n",
       "[:, :, 1] =\n",
       " 1.0  5.0   9.0\n",
       " 2.0  6.0  10.0\n",
       " 3.0  7.0  11.0\n",
       " 4.0  8.0  12.0\n",
       "\n",
       "[:, :, 2] =\n",
       " 13.0  17.0  21.0\n",
       " 14.0  18.0  22.0\n",
       " 15.0  19.0  23.0\n",
       " 16.0  20.0  24.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "(U,C) = HOSVD(T);\n",
    "tau(U,C)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While the matrices $U^\\mu$ are by direct construction orthogonal, the core itself certainly has an interesting property *(why?)* that does not come by chance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2 Array{Float64,2}:\n",
       " 4893.13         1.04453e-12\n",
       "    1.04453e-12  6.86677    "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "C_1 = mu_unfold(1,C);\n",
    "C_1*C_1'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2 Array{Float64,2}:\n",
       " 4852.84         -2.71886e-14\n",
       "   -2.71886e-14  47.1608     "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "C_2 = mu_unfold(2,C);\n",
    "C_2*C_2'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2 Array{Float64,2}:\n",
       " 4849.04          4.52746e-13\n",
       "    4.52746e-13  50.9594     "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "C_3 = mu_unfold(3,C);\n",
    "C_3*C_3'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Representation based operations\n",
    "\n",
    "One of the frequent assumption in numerical, multilinear algebra is that we may be given a decomposition $T = \\tau(U,C)$, but in contrast to $(U,C)$, the tensor $T$ was actually too large to ever allow the call of `tau(U,C)`.\n",
    "\n",
    "Nonetheless, many operations initially declared on a tensor $T$ may be evaluated solely using the representation $(U,C)$. For example, assume we want to know the sum over all entries in $T$,\n",
    "$$ s := \\sum_{i_1 = 1}^{n_1} \\ldots \\sum_{i_d = 1}^{n_d} T_{i_1,\\ldots,i_d}. $$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 10 (theory) </font>\n",
    "\n",
    "Write the sum over all entries $s$ as its own decomposition $s = \\tau(\\hat{U},C) \\in \\mathbb{R}$ by determining the simple relations between $U^\\mu$ and $\\hat{U}^\\mu \\in \\mathbb{R}^{1 \\times r_\\mu}$, for $\\mu = 1,\\ldots,d$. You can find the solution in `solutions/Exercise_1_10.jpynb`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 11 (coding) </font>\n",
    "\n",
    "Implement the idea presented in the prior exercise and test it on an order $4$ tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.3",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.3"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
