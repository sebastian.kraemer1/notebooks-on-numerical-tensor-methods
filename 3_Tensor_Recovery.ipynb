{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tensor Recovery"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"preamble.jl\"); # be sure to run this segment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider we are interested in a certain low rank tensor\n",
    "\n",
    "$$ T^\\ast \\in \\mathcal{V}_{\\leq r} = \\{ T \\in \\mathbb{R}^{n_1 \\times  \\ldots \\times n_d} \\mid \\mathrm{rank}(T^{(\\mu)}) \\leq r_\\mu,\\ \\mu = 1,\\ldots,d \\},$$\n",
    "\n",
    "but we are only given comparitively few linear observations of that tensor. That is, for a linear operator \n",
    "$$\\mathcal{L}: \\mathbb{R}^{n_1 \\times \\ldots \\times n_d} \\rightarrow \\mathbb{R}^{\\ell}$$\n",
    "\n",
    "as well as the vector $y = \\mathcal{L}(T^\\ast)$, we want to recover $T^\\ast$ even though $\\ell < n_1 \\ldots n_d$.\n",
    "\n",
    "The first thing to observe is that this should in principle be possible as long as the number of measurements $\\ell$ is larger than the dimension of the variety $\\mathcal{V}_{\\leq r}$, while we may have yet no idea how to do so.\n",
    "\n",
    "In the following, we will construct a simple Alternating Least Squares (ALS) algorithm in order approach this problem.\\\n",
    "In the last section, you will find a quite different approach called Iteratively Reweighted Least Squares (IRLS). \n",
    "\n",
    "For simplicity, we will assume that the number of measurements $\\ell \\in \\mathbb{N}$ truely provides a unique solution. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Formulation as minimization\n",
    "\n",
    "Earlier, we have learned that we can describe the given variety as the image of a representation map be denote with $\\tau$.\\\n",
    "In simpler terms, for each $T \\in \\mathcal{V}_{\\leq r}$, there exists at least one Tucker decomposition $(U,C)$ for which $T = \\tau(U,C)$.\n",
    "\n",
    "Fortunately, this provides us with a practically quite convenient description as we may now ask not to find $T^\\ast$ directly, but to solve $y = \\mathcal{L}(U^\\ast,C^\\ast)$, whereby then $T^\\ast = \\tau(U^\\ast,C^\\ast)$ will hold true.\n",
    "\n",
    "As often in numerics, it might not be possible to solve such a problem *directly*, but to first relax it into a problem that we can iteratively improve upon.\\\n",
    " Therefor, let us consider the minimization of the euclidean distance\n",
    "\n",
    "$$ f(U,C) := \\| \\mathcal{L}(\\tau(U,C)) - y \\|_F. $$\n",
    "\n",
    "Though we know that we are theoretically able to solve $f(U,C) = 0$, but our perspective has changed greatly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Alternating Least Squares\n",
    "\n",
    " Let us assume we are given some guess $(U,C)$. How can we improve this guess in the sense of reducing the distance $f(U,C)$?\n",
    "\n",
    " We may try to change all of $(U,C)$ at the same time. While there are well understood methods to do so, they do require more elaborate technices *(such as minimization on manifolds)*. \n",
    "\n",
    " A more simplistic approach emerges from the following idea: in each step, fixate all but one of the components of $((U^1,\\ldots,U^d),C)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 1 (theory) </font>\n",
    "\n",
    "Which useful property has each the map from one component to the whole represented tensor? Roughly how can the arising subproblems be solved?\n",
    "\n",
    "We will continue to specifiy an answer to these questions below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Float64,2}:\n",
       "  0.222692   0.420544   0.194263\n",
       "  3.62576    3.90895    3.95858 \n",
       " -0.32399   -0.317695  -0.362287"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "r = (2,2,2);\n",
    "n = (3,3,3);\n",
    "\n",
    "U = [randn(n[1],r[1]),randn(n[2],r[2]),randn(n[3],r[3])];\n",
    "\n",
    "C1 = randn(r);\n",
    "T1 = tau(U,C1);\n",
    "\n",
    "C2 = randn(r);\n",
    "T2 = tau(U,C2);\n",
    "\n",
    "(T1 + 2*T2)[:,:,1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Float64,2}:\n",
       "  0.222692   0.420544   0.194263\n",
       "  3.62576    3.90895    3.95858 \n",
       " -0.32399   -0.317695  -0.362287"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tau(U,C1+2*C2)[:,:,1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Solving ALS subproblems\n",
    "\n",
    "Not only is each the map from one component to the whole tensor linear (after all, this is what multilinear defines), but the decomposition with $\\mathcal{L}$ will be linear as well.\n",
    "\n",
    " As opposed to a multilinear problem, we can easily solve linear problems successively. Let us denote these linear maps as\n",
    " \n",
    " $$ \\tau_{\\setminus U^\\mu}: \\mathbb{R}^{n_mu \\times r_\\mu} \\rightarrow \\mathcal{V}_{\\leq r}, \\quad \\tau_{\\setminus U^\\mu}(U^\\mu) := \\tau(U,C), \\quad \\mu = 1,\\ldots,d, $$\n",
    "\n",
    " as well as\n",
    "\n",
    " $$ \\tau_{\\setminus C}: \\mathbb{R}^{r_1 \\times \\ldots \\times r_d} \\rightarrow \\mathcal{V}_{\\leq r}, \\quad \\tau_{\\setminus C}(C) := \\tau(U,C). $$\n",
    "\n",
    "With the collection of maps $((\\tau_{\\setminus U^1},\\ldots,\\tau_{\\setminus U^d}),\\tau_{\\setminus C})$, we are provided with everything specific to numerical, multilinear algebra."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Practical formulation\n",
    "\n",
    "Assume $\\mathcal{L}: \\mathbb{R}^{n_1 \\times \\ldots \\times n_d} \\rightarrow \\mathbb{R}^{\\ell}$ is given in form of the matrix $L \\in \\mathbb{R}^{\\ell \\times (n_1 \\ldots n_d)}$ subject to the colexicographically sorted canonical basis (that is, the herein usual one).\n",
    "\n",
    "We define a function that sets the desired problem *(this function does not need an output since it overrides the variables found outside of the function; hence the keyword global)*. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 70,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "set_problem (generic function with 1 method)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "function set_problem(n, r, ell_to_dim_V)\n",
    "    global d, dimV, ell, L, U_ast, C_ast, T_ast, y, f\n",
    "\n",
    "    d = length(n)\n",
    "    dim_V = sum(r .* n) + prod(r) - sum(r .* r)\n",
    "\n",
    "    ell = min(prod(n), Int(ceil(dim_V * ell_to_dim_V)))\n",
    "    @printf(\"ell = %d, dim_V = %d \\n\",ell,dim_V)\n",
    "\n",
    "    L = randn(ell, prod(n))\n",
    "\n",
    "    U_ast = map(nr -> randn(nr), zip(n, r))\n",
    "    C_ast = randn(r)\n",
    "    T_ast = tau(U_ast, C_ast)\n",
    "\n",
    "    y = L * T_ast[:]\n",
    "\n",
    "    f = (U,C) -> norm(L*tau(U,C)[:]-y);\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calling `set_problem` will now assign values to the variables. In that function, you can see the dimension of the variety $\\mathcal{V}_{\\leq r}$.\n",
    "\n",
    " While theoretically possible, our algorithm will need some more measurement than just one additional. We call this factor `ell_to_dim_V`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 71,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ell = 96, dim_V = 32 \n"
     ]
    }
   ],
   "source": [
    "n = (4, 4, 4, 4);\n",
    "r = (2, 2, 2, 2);\n",
    "ell_to_dim_V = 3.0;\n",
    "\n",
    "set_problem(n, r, ell_to_dim_V);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 72,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "f(U_ast,C_ast)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subproblems in C\n",
    "\n",
    "The single emerging linear problems can for example be solved via the normal equation. For example, when we rephrase the linear map $\\mathcal{L} \\circ \\tau_{\\setminus C}$ as the matrix \n",
    "$A_{\\setminus C} \\in \\mathbb{R}^{\\ell \\times (r_1 \\ldots r_d)}$, then \n",
    "$$ A_{\\setminus C}^T A_{\\setminus C}\\ \\mathrm{vec}(C^+) = A_{\\setminus C}^T y $$\n",
    "provides the solution to \n",
    "$$ C^+ = \\mathrm{argmin}_{C \\in \\mathbb{R}^{r_1 \\times \\ldots \\times r_d}}\\ f(U,C). $$\n",
    "\n",
    "We may thus have found a good update in one component of $(U,C)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #006165> Exercise 2 (theory) </font>\n",
    "\n",
    "How can $G \\in \\mathbb{R}^{r_1 \\times \\ldots \\times r_d}$, $\\mathrm{vec}(G) := A^T_{\\setminus C} y$, be written using the $\\times_\\mu$-product and by reshaping of tensors? \n",
    "\n",
    "How can $A^T_{\\setminus C}$ be calculated on its own?\n",
    "\n",
    "You will find a solution in `solutions/Exercise_3_2.ipynb`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 3 (coding) </font>\n",
    "\n",
    "The function `A_no_C(L,U)` expects an array `U` of matrices such as below and the matrix `L`, and returns the matrix $A_{\\mathcal{L},U}$.\n",
    "\n",
    "Open the file `my/my_A_no_C.jl` and fill in the missing parts yourself to obtain your own version. Use `mu_mode_prod` or your own code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "metadata": {},
   "outputs": [],
   "source": [
    "U = map(nr->randn(nr),zip(n,r));\n",
    "C = randn(r);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We ask `A_no_C` to return $A_{\\setminus C}$ and then apply it to $\\mathrm{vec}(C)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×96 Adjoint{Float64,Array{Float64,1}}:\n",
       " 28.8334  -22.2036  -19.7539  -23.5794  …  67.7099  15.4634  -35.1299"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A = A_no_C(L,U)\n",
    "( A*C[:] )'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This should give them same as $L$ applied to $\\mathrm{vec}(\\tau(U,C))$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 75,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×96 Adjoint{Float64,Array{Float64,1}}:\n",
       " 28.8334  -22.2036  -19.7539  -23.5794  …  67.7099  15.4634  -35.1299"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "( L*tau(U,C)[:] )'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subproblems in U\n",
    "\n",
    "What remains are the according matrices $A_{\\setminus U^\\mu}$ for $\\mu = 1,\\ldots,d$. At this point, our general approach can become quite cumbersome.\n",
    "\n",
    "One would rather like to reinterpret prior formulations as tensor networks in order to work with them accordingly and to derive calculations.\\\n",
    "Just drawing such may already help, while there are even toolboxes which are specifically tailored to function from the perspective of tensor networks.\n",
    "\n",
    "So while you may try yourself, it is recommended to use the provided functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "mu = 3\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "1×96 Adjoint{Float64,Array{Float64,1}}:\n",
       " 28.8334  -22.2036  -19.7539  -23.5794  …  67.7099  15.4634  -35.1299"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mu = rand(1:ndims(C));\n",
    "println(\"mu = $mu\")\n",
    "\n",
    "A = A_no_U_mu(mu,L,U,C)\n",
    "( A*U[mu][:] )'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×96 Adjoint{Float64,Array{Float64,1}}:\n",
       " 28.8334  -22.2036  -19.7539  -23.5794  …  67.7099  15.4634  -35.1299"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "( L*tau(U,C)[:] )'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Implementing Alternating Least Squares\n",
    "\n",
    "We have everything needed for a simple Alternating Least Squares (ALS) Algorithm now. The idea is to take a possibly generic initial guess and in each *sweep* to successively optimize all components one at a time.\n",
    "\n",
    "Such sweeps are then repeated for a certain number of iterations, or as often used in practice, until a certain accuracy is reached in form of a relatively low value of $f(U,C)$.\n",
    "\n",
    "Note however that sometimes, ALS will not find a satisfactory solution as finding the global minimum of $f(U,C)$ is not guaranteed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <font color = #612158> Exercise 4 (coding / experiment) </font>\n",
    "\n",
    "The function `ALS(L,y,U,C)` expects the matrix `L`, the vector `y`, as well as an initial guess `(U,C)`, and returns an updated decomposition `(U,C)`.\\\n",
    " Optionally, you may specify the maximal number of iterations `iter_max` or an accuracy `tol`.\n",
    "\n",
    "Open the file `my/my_ALS.jl` and fill in the missing parts yourself to obtain your own version. Use `A_no_C` and `A_no_U_mu` or your own code.\\\n",
    "You will find below that optional input has a quite natural syntax.\n",
    "\n",
    "Experiment with other values of $n$, $r$ and in particular `ell_to_dim_V > 1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ell = 96, dim_V = 32 \n",
      "Initial guess: f(U,C) = 8.16e+02 \n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Optimized: f(U,C) = 4.06e+02 \n"
     ]
    }
   ],
   "source": [
    "n = (4, 4, 4, 4);\n",
    "r = (2, 2, 2, 2);\n",
    "ell_to_dim_V = 3.0;\n",
    "\n",
    "set_problem(n, r, ell_to_dim_V);\n",
    "\n",
    "U = map(nr->randn(nr),zip(n,r));\n",
    "C = randn(r);\n",
    "\n",
    "@printf(\"Initial guess: f(U,C) = %.2e \\n\",f(U,C))\n",
    "\n",
    "(U,C) = ALS(L,y,U,C; iter_max=1000, tol=1e-12)\n",
    "\n",
    "@printf(\"Optimized: f(U,C) = %.2e \\n\",f(U,C))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 79,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×96 Adjoint{Float64,Array{Float64,1}}:\n",
       " -51.7913  10.424  55.3145  -14.2928  …  -76.8317  46.9742  -23.7597"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "( L*tau(U,C)[:] )'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 80,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×96 Adjoint{Float64,Array{Float64,1}}:\n",
       " -19.6808  -17.214  -48.3495  -95.2284  …  -65.1404  63.6256  -25.5302"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "y'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a notable difference between minimizing $f(U,C)$ and solving the problem to recover $T^\\ast$. As our premise was that $f(U,C) = 0$ should solve the latter problem, there is nothing more we can do from that point of view. Now, if that premise is actually sensible greatly depends on the problem. \n",
    "\n",
    "Due to the generic way *(though this is something to still argue about)* in which we generate our problem set should however guarantee the unique solvability if only `ell_to_dim_V > 1` holds true. In short, this means that $f(U,C) = 0$ *(almost always)* implies $T = T^\\ast$.\n",
    "\n",
    "Note however that this would not necessarily hold true if we were to randomly pick some $L$ in a non-generic subset of what we currently allow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 81,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×10 Adjoint{Float64,Array{Float64,1}}:\n",
       " -0.862441  -1.30857  -1.45737  -1.01048  …  -0.23227  -0.30108  -0.586725"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tau(U,C)[1:10]'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 82,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×10 Adjoint{Float64,Array{Float64,1}}:\n",
       " -1.5783  1.07538  -4.99558  1.3839  …  -0.857704  -1.67865  1.05692"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tau(U_ast,C_ast)[1:10]'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "## Iteratively Reweighted Least Squares\n",
    "\n",
    "We have seen so far that ALS provides a straightforward, even if slightly technical to implement method. Its convergence properties are however often not satisfactory as it tends to get stuck in local minima as the closer $\\ell$ is chosen to $\\mathrm{dim}(V_{\\leq r})$, the more local minima will appear - even though the problem remains theoretically uniquely solvable.\n",
    "\n",
    " Below, you will find another approach to the initial recovery problem. While the basic idea is not too complicated, there are some more details to it which we will not discuss here.\n",
    "\n",
    "You are invited to experiment with the method and to compare it to the ALS algorithm constructed above. While it may certainly appear cryptic without further comments, it on the other hand has a quite short implementation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 83,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ell = 96, dim_V = 32 \n"
     ]
    }
   ],
   "source": [
    "n = (4, 4, 4, 4);\n",
    "r = (2, 2, 2, 2);\n",
    "ell_to_dim_V = 3.0; \n",
    "\n",
    "set_problem(n, r, ell_to_dim_V);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 84,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Suggested number of iterations: 100 \n"
     ]
    }
   ],
   "source": [
    "iter_max = min(5000,Int(ceil(400/(ell_to_dim_V-1)^2)));\n",
    "@printf(\"Suggested number of iterations: %d \\n\",iter_max)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "256"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T = randn(n);\n",
    "I = map(k->eye(Int,k,k),n);\n",
    "display(prod(n))\n",
    "\n",
    "for iter = 1:iter_max\n",
    "    global T\n",
    "    gamma = 10*1e-10^(iter/iter_max);\n",
    "\n",
    "    # weight matrices\n",
    "    W = zeros(prod(n),prod(n));\n",
    "    for mu = 1:d\n",
    "        S = svd(mu_unfold(mu,T));\n",
    "        W_mu = S.U*Diagonal((S.S.^2 .+ gamma).^(-1))*S.U';\n",
    "        W = W + kron(map(i->(i == mu ? W_mu : I[i]),1:d)[d:-1:1]...);\n",
    "    end\n",
    "\n",
    "    invW_LT = W \\ L';\n",
    "    L_invW_LT = L*invW_LT;\n",
    "    T[:] = invW_LT * (L_invW_LT \\ y);\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, we have not directly calculated a representation $(U,C)$ of the result $T$ (though it is possible to run a version of the algorithm directly on a low rank representation).\n",
    "\n",
    "However, there is a very close relation to the HOSVD. Looking at the singular values of the matricization reveals this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 86,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Array{Float64,1},1}:\n",
       " [98.0558, 47.8143, 8.31315e-11, 7.40335e-11]\n",
       " [96.62, 50.653, 9.09614e-11, 7.25483e-11]   \n",
       " [106.178, 25.0462, 8.06007e-11, 7.65528e-11]\n",
       " [95.8091, 52.1706, 7.8348e-11, 6.95792e-11] "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sigma_T = map(mu->svd(mu_unfold(mu,T)).S,1:d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us again compare $T$ and $T^\\ast$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 87,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×10 Adjoint{Float64,Array{Float64,1}}:\n",
       " 8.6245  1.45017  -0.391535  -2.75724  …  4.60106  -7.0598  -23.5015  -5.0651"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T[1:10]'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×10 Adjoint{Float64,Array{Float64,1}}:\n",
       " 8.6245  1.45017  -0.391535  -2.75724  …  4.60106  -7.0598  -23.5015  -5.0651"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T_ast[1:10]'"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.3",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.3"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
