function mu_fold(k, mu, M)
    d = length(k)

    R = reshape(M, k[[mu; 1:mu-1; mu+1:d]])
    T = permutedims(R, invperm([mu; 1:mu-1; mu+1:d]))

    return T
end