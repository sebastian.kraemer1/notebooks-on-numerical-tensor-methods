function mu_unfold(mu, T)
    k = size(T)
    d = ndims(T)

    # permute mode mu to first position
    P = permutedims(T, [mu; 1:mu-1; mu+1:d]) 

    # reshape permuted tensor into a matrix
    T_mu = reshape(P, k[mu], prod(k[[1:mu-1; mu+1:d]])) 

    return T_mu
end