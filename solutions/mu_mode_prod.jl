function mu_mode_prod(mu, M, T)

    # first the unfolding
    R = mu_unfold(mu, T)

    # matrix times matrix multiplication
    R = M * R

    # undo the unfolding
    k = size(T)
    k = (k[1:mu-1]..., size(M, 1), k[mu+1:end]...)
    MT = mu_fold(k, mu::Int, R)

    return MT
end
