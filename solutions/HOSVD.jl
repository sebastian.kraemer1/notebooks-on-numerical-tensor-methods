function HOSVD(T)
    d = ndims(T)
    U = Array{Array{Float64,2}}(undef, d)

    for mu = 1:d
        (U[mu], s_mu, _) = svd(mu_unfold(mu, T))
        nv = s_mu / norm(s_mu) .> eps()
        U[mu] = U[mu][:, nv]
    end

    C = T

    for mu = 1:d
        C = mu_mode_prod(mu, U[mu]', C)
    end

    return (U, C)
end