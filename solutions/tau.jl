function tau(U, C)
    d = ndims(C)

    T = C;
    for mu = 1:d
        T = mu_mode_prod(mu, U[mu], T)
    end

    return T
end