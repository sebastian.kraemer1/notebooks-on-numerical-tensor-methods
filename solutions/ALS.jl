function ALS(L, y, U, C; iter_max=1000, tol=1e-12)
    norm_y = norm(y)

    for iter = 1:iter_max
        A = A_no_C(L, U)
        C[:] = (A' * A) \ A' * y
        for mu = 1:d
            A = A_no_U_mu(mu, L, U, C)
            U[mu][:] = (A' * A) \ A' * y
        end

        if f(U, C) / norm_y < tol
            break
        end
    end

    return (U, C)
end