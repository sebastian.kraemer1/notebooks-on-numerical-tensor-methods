function A_no_C(L, U)
    d = length(U)
    ell = size(L, 1)
    n = map(M -> size(M, 1), U)

    A = reshape(L', n..., ell)
    for mu = 1:d
        A = mu_mode_prod(mu, U[mu]', A)
    end

    A = reshape(A, prod(i -> size(A, i), 1:d), ell)'

    return A
end