function A_no_U_mu(mu, L, U, C)
    d = length(U)
    ell = size(L, 1)
    n = map(M -> size(M, 1), U)
    r = size(C)

    H = reshape(L', n..., ell)

    for i = [1:mu-1; mu+1:d]
        H = mu_mode_prod(i, U[i]', H)
    end

    # H now corresponds to -> (1...mu-1,n_mu,mu+1..d,ell) (ommiting r_)

    PA = permutedims(H, [d + 1; mu; 1:mu-1; mu+1:d]) # -> (ell,n_mu,1...mu-1,mu+1...d)
    PC = permutedims(C, [1:mu-1; mu+1:d; mu]) # -> (1...mu-1,mu+1...d,r_mu)

    s = prod(r[[1:mu-1; mu+1:d]])
    RA = reshape(PA, ell * n[mu], s) # -> ((ell,n_mu) , (1...mu-1,mu+1...d))
    RC = reshape(PC, s, r[mu]) # -> ((1...mu-1,mu+1...d), r_mu)

    A = reshape(RA * RC, ell, n[mu] * r[mu]) # -> (ell , (n_mu,r_mu))

    return A
end