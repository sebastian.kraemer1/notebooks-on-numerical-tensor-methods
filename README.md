# Notebooks on numerical tensor methods

In this repository, you will find an series of interactive, so called *Jupyter notebooks* on a basic introduction to numerical tensor methods using the programming language *Julia*. 

#### About this material

The series contains
 - 0_Julia: *comprehensive guide on tensor relevant functionality in Julia*
 - 1_HOSVD: *derivation and implementation of Higher Order Singular Value Decomposition*
 - 2_Hierarchical_Tucker: *short introduction to the idea behind the Hierarchical Tucker format*
 - 3_Tensor_Recovery: *basic tensor recovery through Alternating Least Squares and a demonstration of Iteratively Reweighted Least Squares*

You may choose different approaches to the material. At any time you can either solve exercises yourself or, when of interest, read through its frequently provided solutions.\
It is up to you to decide when to do so is best.

If you are not interested in programming whatsoever, you may read all code just as unconventional mathematical formulas.\
Note that the code and in particular its output serve both as explanation and exercise itself, so there will often be few comments to it.

The code itself is kept simple and suggestive - not necessarily how things should be done from a programming perspective.\
Moreover, we will rather immitate mathematical descriptions that allow the most basic implementation from a classical point of view - instead of utilizing on existing implementations which do so much more conveniently.

If you have not already done so, you only need to click the badge below, which will open the link in your browser. The next thing to do within the provided environment will be to open the file `0_Julia.ipynb` and continue there. 

#### About Jupyter notebooks

Whenever you see a code segment, you can select it and run it by either:
 - clicking on the blue line on the left of the block
 - or by pressing *ctrl+enter*
 - or by pressing *shift+enter*  which will additionally switch to the next segment (in order to repeat this thereon) 

though you only need to do so to run your own changes. You may also add new segments when desired.

Note that the order in which you run the code segments determines the order in which the notebook processes the code - not the order in which *you* see the code on the screen.\
Be sure to run top most segment first and in particular rerun it when you for example change the files in `my/`.

#### One last thing before you start

Be sure to check if on the top right corner, it says something along the lines of *Julia kernel*. If *no kernel selected* appears, click and select Julia again. You might need to do so again while working on the notebooks.

You can also restart and rerun the whole document through the buttons on top of the environment. This will not undo your written changes.\
If you have made changes, be sure to save by downloading once in a while, because you may reupload these changes if something goes wrong.

-> [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fsebastian.kraemer1%2Fnotebooks-on-numerical-tensor-methods/main)

*Note that clicking the link may sometimes not work. If so, copy it in your browser manually. If you already use Julia on your own computer, you may also directly clone the repository and work offline.*
