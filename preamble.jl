# basic linear algebra operations
using LinearAlgebra

# C/Matlab style print
using Printf

# eye as in Matlab
function eye(dims...)
    return eye(Float64,dims...)
end

# eye as in Matlab with non default type
function eye(T::Type, dims...)
    A = zeros(T,dims);
    m = min(dims...);
    for i = 1:m
        A[repeat([i],ndims(A))...] = 1.0;
    end
    return A
end

# reshape accepting arrays
import Base.reshape
function reshape(A,v::Array{Int,1})
    return reshape(A,v...);
end

# solutions/
### 1_HOSVD
include("solutions/mu_unfold.jl");
include("solutions/mu_fold.jl");
include("solutions/mu_mode_prod.jl");
include("solutions/tau.jl");
include("solutions/HOSVD.jl");

### 3_Tensor_Recovery
include("solutions/A_no_C.jl");
include("solutions/A_no_U_mu.jl");
include("solutions/ALS.jl");

# my/
### 1_HOSVD
include("my/my_mu_unfold.jl");
include("my/my_mu_fold.jl");
include("my/my_mu_mode_prod.jl");
include("my/my_tau.jl");
include("my/my_HOSVD.jl");

### 3_Tensor_Recovery
include("my/my_A_no_C.jl");
include("my/my_A_no_U_mu.jl");
include("my/my_ALS.jl");
