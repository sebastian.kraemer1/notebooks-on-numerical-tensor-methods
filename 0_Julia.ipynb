{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Julia"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 225,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"preamble.jl\"); # be sure to run this segment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "The chapters in this notebook are:\n",
    " - Tuples\n",
    " - Arrays\n",
    " - Ranges\n",
    " - Vectors\n",
    " - Matrices\n",
    " - Tensors\n",
    " - For-loops\n",
    " - Functions\n",
    " - Map\n",
    " - Reshape\n",
    " - Permute\n",
    "\n",
    "This notebook provides an overview over some basic Julia syntax that we require in this series. Fortunately, the language provides a very close translation of mathematics, so much will be intuitive.\n",
    "\n",
    "**Navigating you here was just to make sure that you are aware of this notebook. Open `1_HOSVD.ipynb` next and return when something is unclear.**\n",
    "\n",
    "If you want, just go ahead, though nothing here is really specific to numerical tensor methods. There are also no exercises in this notebook.\\\n",
    "You do not need to manually run the code segments as long as you do not want to change anything. Playing around with code however is greatly encouraged.\n",
    "\n",
    "When you are experienced with Julia, or even just programming in general, you might also very quickly read through this notebook.\\\n",
    "Be aware of some pitfalls when you are used to Matlab or Python for example. There is a good summary provided on https://docs.julialang.org/en/v1/manual/noteworthy-differences/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    " \n",
    "### Tuples\n",
    "\n",
    "`Tuple`'s are pretty much in Julia what tuples are in mathematics, just an ordered set of anything (or something more specific)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 226,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, 2, \"three\", 4, \"five\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "t = (1,2,\"three\",4,\"five\") # after running this segment (and only then), t is known by Julia as what we assigned to it"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can ask for some part of `t`..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 227,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "t[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 228,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, \"three\", 4)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "t[2:4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...but we may never change an existing tuple. \n",
    "\n",
    "Though this does not mean that we could not make new tuples or reassign a new tuple to the mere symbol `t`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 229,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, (\"three\", 4))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "(t[2],t[3:4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we now have a tuple where the second entry is itself a tuple. If we want to have actual $3$-tuple, we quite literally need to remove brackets using `...` as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 230,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, \"three\", 4)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "(t[2],t[3:4]...) # the operation `...` is quite similar to Maple's command op()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Arrays\n",
    "\n",
    "Now, what Julia knows as a one-dimensional `Array` is also an ordered set, *but different*. This is the only technical part we might need to get into since Julia both likes tuples and arrays. Do not worry about each the first line here. This is Julia telling us in a reasonably crypting way what kind of variable *(type)* we have.\n",
    "\n",
    "Take an array for example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 231,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5-element Array{Any,1}:\n",
       " 1       \n",
       " 2       \n",
       "  \"three\"\n",
       " 4       \n",
       "  \"five\" "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "a = [1,2,\"three\",4,\"five\"] # `Any` below means this is an array where each entry could hold anything Julia knows of"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also ask for some part of `a`..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 232,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2-element Array{Any,1}:\n",
       " 2       \n",
       "  \"three\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "a[2:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...but now we can change parts of `a`, in contrast to the tuple `t`! It takes some programming experience though to realize that this drawback of tuples is also one of its features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 233,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5-element Array{Any,1}:\n",
       " 1       \n",
       "  \"two\"  \n",
       "  \"three\"\n",
       " 4       \n",
       "  \"five\" "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "a[2] = \"two\";\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also combine arrays to new ones. Note that, similar as above, we get an array of which one entry is a natural number, and the other one is an array itself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 234,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2-element Array{Any,1}:\n",
       " \"two\"                  \n",
       " Any[\"three\", 4, \"five\"]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[a[2],a[3:5]] "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can either use the same trick as before..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 235,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Any,1}:\n",
       "  \"two\"  \n",
       "  \"three\"\n",
       " 4       \n",
       "  \"five\" "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[a[2],a[3:5]...]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "...or change the `,` to a `;`. Rather do not try this with tuples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 236,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Any,1}:\n",
       "  \"two\"  \n",
       "  \"three\"\n",
       " 4       \n",
       "  \"five\" "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[a[2];a[3:5]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also turn either into the other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 237,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, \"two\", \"three\", 4, \"five\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tuple(a...) # note that `(a...)` won't be enough here..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 238,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, \"two\", \"three\", 4, \"five\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "(a...,) # ...but this will do the trick"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 239,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1, \"two\", \"three\", 4, \"five\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Tuple(a) # this tells Julia to directly convert `a` to the type `Tuple`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are fewer apparent options to turn `t` into an array though."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 240,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5-element Array{Any,1}:\n",
       " 1       \n",
       " 2       \n",
       "  \"three\"\n",
       " 4       \n",
       "  \"five\" "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[t...] # but neither `Array(t)` nor `Array(t...)` will work, while the function `array()` does not even exist (in version 1.0.3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "### Ranges\n",
    "\n",
    "Particularly useful is the command `n1:n2` for any integral numbers `n1` and `n2`, which we have already used above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 241,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2:5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "s = 2:5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 242,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "UnitRange{Int64}"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "typeof(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will ignore that this is yet both something different than the tuple `(2,3,4,5)` and the array `[2,3,4,5]` as we can turn it into either one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 243,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2:5,)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tuple(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This may not yet quite what we want."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 244,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 3, 4, 5)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tuple(s...) # better (for our purposes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 245,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 3, 4, 5)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Tuple(s) # `Tuple()` is something different than `tuple()` - `Tuple` is a type which can always be used as function to convert variables, \n",
    "# while `tuple` is a function to begin with that was manually made to return `Tuple` type objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 246,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Int64,1}:\n",
       " 2\n",
       " 3\n",
       " 4\n",
       " 5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[s...]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 247,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Int64,1}:\n",
       " 2\n",
       " 3\n",
       " 4\n",
       " 5"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Array(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often though, explicitly doing so won't be necessary (in that extend)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 248,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(3, 4, 2, 3)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "(s[2:3]...,s[1:2]...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 249,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Int64,1}:\n",
       " 3\n",
       " 4\n",
       " 2\n",
       " 3"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[s[2:3]; s[1:2]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You however need to be careful with the order of operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 250,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2:4"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "2:5 - 1 # while (2:5) - 1 will unfortunately throw an error, maybe just because it was not implemented yet (in version 1.0.3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Vectors\n",
    "\n",
    "We have just seen vectors above. They are literally one-dimensional arrays in Julia. Throughout these notebooks however, we will rather refer to a vector as something filled with actual numbers.\n",
    "\n",
    "\n",
    "The following will be regarded as programming equivalent of $v_1 := (1,2,3)^T$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 251,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Array{Int64,1}:\n",
       " 1\n",
       " 2\n",
       " 3"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v1 = [1;2;3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a column vector, ommit the `;`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 252,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×3 Array{Int64,2}:\n",
       " 1  2  3"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v2 = [1 2 3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrast, using `'` will never modify the single arguments but collect such in an array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 253,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2-element Array{Any,1}:\n",
       " 1   \n",
       "  2:3"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[1,2:3] # but [1 2:3] will throw an error as it tries to put two columns vector next to each other despite their different lengths (in version 1.0.3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 254,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Array{Int64,1}:\n",
       " 1\n",
       " 2\n",
       " 3"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "[1;2:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many of the operations come quite naturally."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 255,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1-element Array{Int64,1}:\n",
       " 14"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v2*v1 # this is a scalar product between two vectors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 256,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Int64,2}:\n",
       " 1  2  3\n",
       " 2  4  6\n",
       " 3  6  9"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v1*v2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 257,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×1 Array{Int64,2}:\n",
       " 2\n",
       " 4\n",
       " 6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v1 + v2'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also assign $v_3$ an already existing vector - and Julia thinks of $v_3 := v_2$ quite literally here. Changing either of the variables will change the other as well. \n",
    "\n",
    "While this is imperative to know in general programming, we will not explicitly make use of this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 258,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v3 = v2 # copy by reference\n",
    "v2[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are entirely unfamiliar with this concept, think of the arrays `v2` and `v3` as the same addresses of the exact same thing but written at two different places. Copying the address will not necessarily change that thing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 259,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v3[1] = 0;\n",
    "v2[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to change the data `v3` refers to, one first needs to somehow *view* that data. This is what the call `v3[1]` does.\\\n",
    " \n",
    "We may even override all entries `v2` refers to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 260,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×3 Array{Int64,2}:\n",
       " 0  4  9"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v2[:] = v2.*v3 # we are first multiplying the vector data with itself, and then change that very data v2 is pointing towards\n",
    "v2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 261,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×3 Array{Int64,2}:\n",
       " 0  4  9"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see below, this is something different than overriding the address of `v2`. Here, we first establish some data and then change `v2` to refer to that data - instead of overriding what `v3` still refers to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 262,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×3 Array{Int64,2}:\n",
       " 0  16  81"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v2 = v2.*v3 # we are first multiplying the vector data with itself, and once that is done, v2 will point to somewhere else than v3\n",
    "v2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 263,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v3[1] = 10;\n",
    "v2[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One may ask why then `v2.*v3` does not try to multiply the two addresses? This is simply because it was programmed not to, as is near any other command already provided by Julia.\n",
    "\n",
    "Among the exceptions is the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 264,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Ptr{Nothing} @0x00007f64f6ce3960"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pointer_from_objref(v2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 265,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Ptr{Nothing} @0x00007f64f91cc010"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "pointer_from_objref(v3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Matrices\n",
    "\n",
    "There are quite a few commands which will return what we will regard as matrices *(note however, that there is a difference between an array of arrays - and a two-dimensional array. Further, Julia like Matlab acts column-first in contrast to Python's numpy)*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 266,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×3 Array{Int64,2}:\n",
       " 0  0  0\n",
       " 0  0  0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A1 = zeros(Int,2,3) # `Int` here tells Julia to interpret the numbers in `T1` as integral numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 267,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Float64,2}:\n",
       " 1.0  1.0  1.0\n",
       " 1.0  1.0  1.0\n",
       " 1.0  1.0  1.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A2 = ones(3,3) # without `Int`, the entries of `A2` will be floating point numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 268,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×3 Array{Float64,2}:\n",
       " 1.0  0.0  0.0\n",
       " 0.0  1.0  0.0\n",
       " 0.0  0.0  1.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A3 = eye(3,3) # eye is a custom function within `preamble.jl` (that behaves like Matlab's eye except that eye(3) will actually be a vector)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 269,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A3[1,3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 270,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "A3[2,2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 271,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3×2 Array{Float64,2}:\n",
       " 1.0  0.0\n",
       " 0.0  1.0\n",
       " 0.0  0.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "eye(3,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Tensors\n",
    "\n",
    "Tensors may come in all finite shapes and sizes in Julia (but they always come with a colexicographically sorted, canonical basis), and they generally follow the same commands as for matrices as such are just order 2 tensors *(which are all just higher dimensional arrays in Julia)*.\n",
    "\n",
    "Often order and dimension are used synonymously, but the term order comes with the advantage that it is seldomly synonymously used as yet something else within the context of tensors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 272,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×3×4 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 0  0  0\n",
       " 0  0  0\n",
       "\n",
       "[:, :, 2] =\n",
       " 0  0  0\n",
       " 0  0  0\n",
       "\n",
       "[:, :, 3] =\n",
       " 0  0  0\n",
       " 0  0  0\n",
       "\n",
       "[:, :, 4] =\n",
       " 0  0  0\n",
       " 0  0  0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T1 = zeros(Int,2,3,4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 273,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×3×4 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 1  3  5\n",
       " 2  4  6\n",
       "\n",
       "[:, :, 2] =\n",
       " 7   9  11\n",
       " 8  10  12\n",
       "\n",
       "[:, :, 3] =\n",
       " 13  15  17\n",
       " 14  16  18\n",
       "\n",
       "[:, :, 4] =\n",
       " 19  21  23\n",
       " 20  22  24"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T1[:] = 1:length(T1)\n",
    "T1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are often many ways to achieve the same. For example, when we want to assign specific entries to a tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 274,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "24"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T3 = zeros(Int,4,3,2) \n",
    "number_of_entries_in_T3 = length(T3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 275,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4×3×2 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       "  1  25   81\n",
       "  4  36  100\n",
       "  9  49  121\n",
       " 16  64  144\n",
       "\n",
       "[:, :, 2] =\n",
       " 169  289  441\n",
       " 196  324  484\n",
       " 225  361  529\n",
       " 256  400  576"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "for i in 1:number_of_entries_in_T3\n",
    "    T3[i] = i^2;\n",
    "end\n",
    "T3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 276,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4×3×2 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       "  1  25   81\n",
       "  4  36  100\n",
       "  9  49  121\n",
       " 16  64  144\n",
       "\n",
       "[:, :, 2] =\n",
       " 169  289  441\n",
       " 196  324  484\n",
       " 225  361  529\n",
       " 256  400  576"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "f = i->i^2;\n",
    "T3[:] = map(f,1:number_of_entries_in_T3);\n",
    "T3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 277,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4×3×2 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       "  1  25   81\n",
       "  4  36  100\n",
       "  9  49  121\n",
       " 16  64  144\n",
       "\n",
       "[:, :, 2] =\n",
       " 169  289  441\n",
       " 196  324  484\n",
       " 225  361  529\n",
       " 256  400  576"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T3[:] = (1:number_of_entries_in_T3).^2;\n",
    "T3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### For-loops\n",
    "\n",
    "As you have seen above, something can be done `for` certain values. There are many ways to achieve this, but we will only need this kind of loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 278,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "2\n",
      "3\n"
     ]
    }
   ],
   "source": [
    "for i = 1:3\n",
    "    println(i);\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Though Julia allows to write this in a few different ways."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 279,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 1)\n",
      "(1, 2)\n",
      "(2, 1)\n",
      "(2, 2)\n",
      "(3, 1)\n",
      "(3, 2)\n"
     ]
    }
   ],
   "source": [
    "for i in 1:3\n",
    "    for j in 1:2\n",
    "        println((i,j));\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 280,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 1)\n",
      "(1, 2)\n",
      "(2, 1)\n",
      "(2, 2)\n",
      "(3, 1)\n",
      "(3, 2)\n"
     ]
    }
   ],
   "source": [
    "for i in 1:3, j in 1:2\n",
    "    println((i,j));\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Functions\n",
    "\n",
    "While there are many subtleties to functions, they are in their essence the same as in mathematics. They often require specific input and return the value of interest.\n",
    "\n",
    "Here, it would be better to say that there are *similar* ways to do so as, in contrast to before, the different ways to do so may indeed construct slightly different things (which for these notebook will not matter much though)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 281,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "f1 = x->x+x^2\n",
    "f1(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 282,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "f2(x) = x+x^2\n",
    "f2(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 283,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "function f3(x)\n",
    "    return x+x^2\n",
    "end\n",
    "f3(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are comparably simple ways to work with specified types of input in Julia as well, but we will not need this. You may find more about this under the phrase `method`.\n",
    "\n",
    "Just to give a minimal example, observe the following."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 284,
   "metadata": {},
   "outputs": [],
   "source": [
    "is_the_type_of_this_number(x::Any) = false;\n",
    "is_the_type_of_this_number(x::Number) = true;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 285,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "false"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "is_the_type_of_this_number(\"three\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 286,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "true"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "is_the_type_of_this_number(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Map\n",
    "\n",
    "Just to mention it - we can combine the idea of a for-loop with the evaluation of a function in a very quick way.\n",
    "\n",
    "Note however that `map` is itself just a versatile, ordinary `function`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 287,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Array{Int64,1}:\n",
       "  1\n",
       "  4\n",
       "  9\n",
       " 16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "g = x->x^2;\n",
    "map(g,1:4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Reshape\n",
    "\n",
    "In the course of these notebooks, we in particular want to manipulate the entries of tensors. For example, let there be a simple vector as below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 288,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1×16 Adjoint{Int64,Array{Int64,1}}:\n",
       " 1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "v = Array(1:2*4*2);\n",
    "display(v') # show the transpose (or more general, the adjoint)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 289,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(16,)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "size(v) # read (12,) just as the 1-tuple (12) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then `reshape` it be specifying the new mode sizes we desire. The total number of entries of course must not change. For example, `reshape(v,(2,2))` would not work and Julia would complain accordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 290,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×4×2 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 1  3  5  7\n",
       " 2  4  6  8\n",
       "\n",
       "[:, :, 2] =\n",
       "  9  11  13  15\n",
       " 10  12  14  16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T4 = reshape(v,(2,4,2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 291,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2, 4, 2)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "size(T4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can certainly ask for a specific entry of $T_4$. We may for example ask for the $15$-th entry *(starting with $1$, not $0$)* of $T_4$ when counting along its entries starting with columns first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 292,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "index = 8;\n",
    "T4[index]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or we can directly ask for a specific entry. Naturally, there is a bijection between these subscripts and aboves indexes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 293,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "subscript = [2,4,1]\n",
    "T4[subscript...] # we need to use ... here, as otherwise we ask for the 2-nd, 4-th as well as 1-st entry of T4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- ---\n",
    "\n",
    "### Permute \n",
    "\n",
    "We can also permute the modes of a tensor (or matrix) using `permutedims`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 294,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2×4 Array{Int64,3}:\n",
       "[:, :, 1] =\n",
       " 1   2\n",
       " 9  10\n",
       "\n",
       "[:, :, 2] =\n",
       "  3   4\n",
       " 11  12\n",
       "\n",
       "[:, :, 3] =\n",
       "  5   6\n",
       " 13  14\n",
       "\n",
       "[:, :, 4] =\n",
       "  7   8\n",
       " 15  16"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "perm = [3,1,2];\n",
    "permuted_T4 = permutedims(T4,perm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we permute the subscript as well, we will again obtain the same entry as for the unpermuted subscript and tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 295,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Array{Int64,1}:\n",
       " 1\n",
       " 2\n",
       " 4"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "permuted_subscript = subscript[perm]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 296,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "permuted_T4[permuted_subscript...]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 297,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "8"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T4[subscript...]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.0.3",
   "language": "julia",
   "name": "julia-1.0"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.0.3"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
